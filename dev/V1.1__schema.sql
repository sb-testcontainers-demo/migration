create sequence hibernate_sequence;

create domain clob as text;

create table users (
  id      bigint  not null constraint users_pkey primary key,
  version integer,
  name    text    not null,
  phones  jsonb,
  roles   jsonb   not null
);